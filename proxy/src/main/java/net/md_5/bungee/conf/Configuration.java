package net.md_5.bungee.conf;

import lombok.Getter;
import lombok.Synchronized;
import net.md_5.bungee.api.config.ConfigurationType;
import net.md_5.bungee.api.config.list.ConfigurationMain;
import net.md_5.bungee.conf.list.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
public class Configuration implements ConfigurationMain {

    private final Object serversLock = new Object();

    private Map<ConfigurationType, ConfigurationAbstract> configData;

    public Configuration() {
        this.configData = new ConcurrentHashMap<>();
    }

    public static ConfigurationAbstract create(ConfigurationType configurationType) {
        switch (configurationType) {
            case PROXY:
                return new ConfigurationAbstractProxy();
            case SERVER:
                return new ConfigurationAbstractServer();
            case LISTENER:
                return new ConfigurationAbstractListener();
            case GROUP:
                return new ConfigurationAbstractGroup();
            case PERMISSION:
                return new ConfigurationAbstractPermission();
            default:
                throw new IllegalArgumentException("Illegal ConfigurationType");
        }
    }

    @Synchronized("serversLock")
    public void load() {
        for (ConfigurationType configurationType : ConfigurationType.values()) {
            ConfigurationAbstract configuration = create(configurationType);
            configuration.load();
            configData.put(configurationType, configuration);
        }
    }

    @Override
    public ConfigurationAbstractProxy getProxyConfig() {
        return (ConfigurationAbstractProxy) this.configData.get(ConfigurationType.PROXY);
    }

    @Override
    public ConfigurationAbstractServer getServerConfig() {
        return (ConfigurationAbstractServer) this.configData.get(ConfigurationType.SERVER);
    }

    @Override
    public ConfigurationAbstractListener getListenerConfig() {
        return (ConfigurationAbstractListener) this.configData.get(ConfigurationType.LISTENER);
    }

    @Override
    public ConfigurationAbstractGroup getGroupConfig() {
        return (ConfigurationAbstractGroup) this.configData.get(ConfigurationType.GROUP);
    }

    @Override
    public ConfigurationAbstractPermission getPermissionConfig() {
        return (ConfigurationAbstractPermission) this.configData.get(ConfigurationType.PERMISSION);
    }
}
