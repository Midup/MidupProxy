package net.md_5.bungee.conf;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ConfigurationAdapter;
import net.md_5.bungee.api.config.ConfigurationType;
import net.md_5.bungee.util.CaseInsensitiveMap;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;

public abstract class ConfigurationAbstract implements ConfigurationAdapter {

    public static final String DIRECTORY_CONFIG = "midup";

    private final Yaml yaml;
    private final @Getter
    File configFile;
    private final @Getter
    ConfigurationType type;

    private @Getter
    Map config;

    public ConfigurationAbstract(File file) {
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        this.yaml = new Yaml(options);
        this.configFile = file;
        this.type = null;
    }

    public ConfigurationAbstract(ConfigurationType type) {
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        this.yaml = new Yaml(options);
        this.configFile = new File(DIRECTORY_CONFIG, type.getConfigFileName());
        this.type = type;
    }

    @Override
    public void load() {
        try {
            configFile.createNewFile();

            try (InputStream is = new FileInputStream(configFile)) {
                config = (Map) yaml.load(is);
            }

            if (config == null) {
                config = new CaseInsensitiveMap();
            } else {
                config = new CaseInsensitiveMap(config);
            }
        } catch (IOException ex) {
            throw new RuntimeException("Could not load configuration!", ex);
        }
    }

    public <T> T get(String path, T def) {
        return get(path, def, config);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String path, T def, Map submap) {
        int index = path.indexOf('.');
        if (index == -1) {
            Object val = submap.get(path);
            if (val == null && def != null) {
                val = def;
                submap.put(path, def);
                save();
            }
            return (T) val;
        } else {
            String first = path.substring(0, index);
            String second = path.substring(index + 1, path.length());
            Map sub = (Map) submap.get(first);
            if (sub == null) {
                sub = new LinkedHashMap();
                submap.put(first, sub);
            }
            return get(second, def, sub);
        }
    }

    @SuppressWarnings("unchecked")
    public void set(String path, Object val, Map submap) {
        int index = path.indexOf('.');
        if (index == -1) {
            if (val == null) {
                submap.remove(path);
            } else {
                submap.put(path, val);
            }
            save();
        } else {
            String first = path.substring(0, index);
            String second = path.substring(index + 1, path.length());
            Map sub = (Map) submap.get(first);
            if (sub == null) {
                sub = new LinkedHashMap();
                submap.put(first, sub);
            }
            set(second, val, sub);
        }
    }

    public void save() {
        try {
            try (FileWriter wr = new FileWriter(configFile)) {
                yaml.dump(config, wr);
            }
        } catch (IOException ex) {
            ProxyServer.getInstance().getLogger().log(Level.WARNING, "Could not save config", ex);
        }
    }

    @Override
    public int getInt(String path, int def) {
        return get(path, def);
    }

    @Override
    public String getString(String path, String def) {
        return get(path, def);
    }

    @Override
    public boolean getBoolean(String path, boolean def) {
        return get(path, def);
    }

    @Override
    public Collection<?> getList(String path, Collection<?> def) {
        return get(path, def);
    }

    @RequiredArgsConstructor
    private enum DefaultTabList {

        GLOBAL(), GLOBAL_PING(), SERVER();
    }
}
