package net.md_5.bungee.conf.list;

import lombok.RequiredArgsConstructor;
import net.md_5.bungee.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.config.ConfigurationType;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.list.ConfigurationListener;
import net.md_5.bungee.conf.ConfigurationAbstract;
import net.md_5.bungee.util.CaseInsensitiveMap;

import java.net.InetSocketAddress;
import java.util.*;

public class ConfigurationAbstractListener extends ConfigurationAbstract implements ConfigurationListener {

    private Collection<ListenerInfo> listeners;

    public ConfigurationAbstractListener() {
        super(ConfigurationType.LISTENER);
    }

    @Override
    public void load() {
        super.load();

        this.listeners = getListeners();
    }

    @Override
    public Collection<ListenerInfo> getListeners() {
        Collection<Map<String, Object>> base = get("listeners", (Collection) Arrays.asList(new Map[]{new HashMap()}));

        Map<String, String> forcedDef = new HashMap<>();
        forcedDef.put("pvp.md-5.net", "pvp");

        Collection<ListenerInfo> ret = new HashSet<>();

        for (Map<String, Object> val : base) {
            String motd = get("motd", "&1Another Bungee server", val);
            motd = ChatColor.translateAlternateColorCodes('&', motd);

            int maxPlayers = get("max_players", 1, val);
            boolean forceDefault = get("force_default_server", true, val);
            String host = get("host", "0.0.0.0:25577", val);
            int tabListSize = get("tab_size", 60, val);
            InetSocketAddress address = Util.getAddr(host);
            Map<String, String> forced = new CaseInsensitiveMap<>(get("forced_hosts", forcedDef, val));
            String tabListName = get("tab_list", "SERVER", val);
            DefaultTabList value = DefaultTabList.valueOf(tabListName.toUpperCase());
            if (value == null) {
                value = DefaultTabList.SERVER;
            }
            boolean setLocalAddress = get("bind_local_address", true, val);
            boolean pingPassthrough = get("ping_passthrough", false, val);

            boolean query = get("query_enabled", false, val);
            int queryPort = get("query_port", 25577, val);

            boolean proxyProtocol = get("proxy_protocol", false, val);
            List<String> serverPriority = new ArrayList<>(get("priorities", Collections.EMPTY_LIST, val));

            String defaultServer = get("default_server", null, val);
            String fallbackServer = get("fallback_server", null, val);
            if (defaultServer != null) {
                serverPriority.add(defaultServer);
                set("default_server", null, val);
            }
            if (fallbackServer != null) {
                serverPriority.add(fallbackServer);
                set("fallback_server", null, val);
            }

            if (serverPriority.isEmpty()) {
                serverPriority.add("HUB");
            }
            set("priorities", serverPriority, val);

            ListenerInfo info = new ListenerInfo(address, motd, maxPlayers, tabListSize, serverPriority, forceDefault, forced, value.toString(), setLocalAddress, pingPassthrough, queryPort, query, proxyProtocol);
            ret.add(info);
        }
        return ret;
    }

    @RequiredArgsConstructor
    private enum DefaultTabList {

        GLOBAL(), GLOBAL_PING(), SERVER();
    }
}
