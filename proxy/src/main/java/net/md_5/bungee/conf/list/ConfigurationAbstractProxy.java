package net.md_5.bungee.conf.list;

import com.google.common.base.Joiner;
import lombok.Getter;
import lombok.Synchronized;
import net.md_5.bungee.api.Favicon;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ConfigurationType;
import net.md_5.bungee.api.config.list.ConfigurationProxy;
import net.md_5.bungee.conf.ConfigurationAbstract;
import net.md_5.bungee.protocol.ProtocolConstants;
import net.md_5.bungee.util.CaseInsensitiveSet;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;
import java.util.logging.Level;

@Getter
public class ConfigurationAbstractProxy extends ConfigurationAbstract implements ConfigurationProxy {

    private final Object serversLock = new Object();

    private int timeout = 30000;
    private String uuid = UUID.randomUUID().toString();

    private boolean onlineMode = true;
    private boolean logCommands;
    private int playerLimit = -1;
    private Collection<String> disabledCommands;
    private int throttle = 4000;
    private boolean ipForward;
    private Favicon favicon;
    private int compressionThreshold = 256;
    private boolean preventProxyConnections;

    private boolean metrics = true;
    private boolean logServerListPing = false;
    private String gameVersion;
    private boolean useNettyDnsResolver = true;
    private int tabThrottle = 1000;

    public ConfigurationAbstractProxy() {
        super(ConfigurationType.PROXY);
    }

    @Override
    @Synchronized("serversLock")
    public void load() {
        super.load();

        File fav = new File("server-icon.png");
        if (fav.exists()) {
            try {
                this.favicon = Favicon.create(ImageIO.read(fav));
            } catch (IOException | IllegalArgumentException ex) {
                ProxyServer.getInstance().getLogger().log(Level.WARNING, "Could not load server icon", ex);
            }
        }

        this.timeout = getInt("timeout", timeout);
        this.uuid = getString("stats", uuid);
        this.onlineMode = getBoolean("online_mode", onlineMode);
        this.logCommands = getBoolean("log_commands", logCommands);
        this.playerLimit = getInt("player_limit", playerLimit);
        this.throttle = getInt("connection_throttle", throttle);
        this.ipForward = getBoolean("ip_forward", ipForward);
        this.compressionThreshold = getInt("network_compression_threshold", compressionThreshold);
        this.preventProxyConnections = getBoolean("prevent_proxy_connections", preventProxyConnections);
        this.disabledCommands = new CaseInsensitiveSet((Collection<String>) getList("disabled_commands", Arrays.asList("disabledcommandhere")));

        this.metrics = getBoolean("metrics", metrics);
        this.logServerListPing = getBoolean("log_server_list_ping", logServerListPing);
        this.tabThrottle = getInt("throttling.tab_complete", tabThrottle);
        this.gameVersion = getString("game_version", "").isEmpty() ? Joiner.on(", ").join(ProtocolConstants.SUPPORTED_VERSIONS) : getString("game_version", "");
        this.useNettyDnsResolver = getBoolean("use_netty_dns_resolver", useNettyDnsResolver);
    }

    @Override
    public Favicon getFaviconObject() {
        return this.favicon;
    }

    @Override
    public boolean isMetrics() {
        return this.metrics;
    }

    @Override
    public boolean isLogServerListPing() {
        return this.logServerListPing;
    }

    @Override
    public int getTabThrottle() {
        return this.tabThrottle;
    }
}
