package net.md_5.bungee.conf.list;

import br.com.midup.proxy.MidupProxy;
import br.com.midup.proxy.api.data.server.ServerType;
import br.com.midup.proxy.data.server.ServerData;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import gnu.trove.map.TMap;
import lombok.Synchronized;
import net.md_5.bungee.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ConfigurationType;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.config.list.ConfigurationServer;
import net.md_5.bungee.conf.ConfigurationAbstract;
import net.md_5.bungee.util.CaseInsensitiveMap;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class ConfigurationAbstractServer extends ConfigurationAbstract implements ConfigurationServer {

    private final Object serversLock = new Object();

    private TMap<String, ServerInfo> servers;

    public ConfigurationAbstractServer() {
        super(ConfigurationType.SERVER);
    }

    @Override
    public void load() {
        super.load();

        Map<String, ServerInfo> newServers = getServers();
        Preconditions.checkArgument(newServers != null && !newServers.isEmpty(), "No servers defined");

        if (servers == null) {
            servers = new CaseInsensitiveMap<>(newServers);
        } else {
            for (ServerInfo oldServer : servers.values()) {
                Preconditions.checkArgument(newServers.containsKey(oldServer.getName()), "Server %s removed on reload!", oldServer.getName());
            }
            for (Map.Entry<String, ServerInfo> newServer : newServers.entrySet()) {
                if (!servers.containsValue(newServer.getValue())) {
                    servers.put(newServer.getKey(), newServer.getValue());
                }
            }
        }
    }

    @Override
    public Map<String, ServerInfo> getServers() {
        Map<String, Map<String, Object>> base = get("servers", Collections.singletonMap("HUB", new HashMap<>()));
        Map<String, ServerInfo> ret = new HashMap<>();

        for (Map.Entry<String, Map<String, Object>> entry : base.entrySet()) {
            Map<String, Object> val = entry.getValue();
            String name = entry.getKey();
            String addr = get("address", "localhost:25565", val);
            String motd = ChatColor.translateAlternateColorCodes('&',
                    get("motd", "&1Just another MidupProxy - Forced Host", val));
            String type = get("type", "MAIN", val);
            String mode = get("mode", "HUB", val);
            boolean restricted = get("restricted", false, val);
            InetSocketAddress address = Util.getAddr(addr);

            ServerType serverType = ServerType.getServerType(type);
            if (serverType == null) {
                ProxyServer.getInstance().getLogger().log(Level.SEVERE, "Unknown server type {0}", type);
                continue;
            }
            ServerType.ServerMode serverMode = ServerType.ServerMode.getServerMode(mode);
            if (serverMode == null) {
                ProxyServer.getInstance().getLogger().log(Level.SEVERE, "Unknown server mode {0}", mode);
                continue;
            }
            ServerInfo info = ProxyServer.getInstance().constructServerInfo(name, address, motd, restricted);
            ServerData serverData = ServerData.create(name, address.getPort(), serverType);
            serverData.getType().setMode(serverMode);
            serverData.setInfo(info);
            MidupProxy.getInstance().getConnectionManager().getServerDatas().put(address.getPort(), serverData);
            ret.put(name, info);
        }
        return ret;
    }

    @Override
    @Synchronized("serversLock")
    public Map<String, ServerInfo> getServersCopy() {
        return ImmutableMap.copyOf(servers);
    }

    @Override
    @Synchronized("serversLock")
    public ServerInfo getServerInfo(String name) {
        return this.servers.get(name);
    }

    @Override
    @Synchronized("serversLock")
    public ServerInfo addServer(ServerInfo server) {
        return this.servers.put(server.getName(), server);
    }

    @Override
    @Synchronized("serversLock")
    public boolean addServers(Collection<ServerInfo> servers) {
        boolean changed = false;
        for (ServerInfo server : servers) {
            if (server != this.servers.put(server.getName(), server)) changed = true;
        }
        return changed;
    }

    @Override
    @Synchronized("serversLock")
    public ServerInfo removeServerNamed(String name) {
        return this.servers.remove(name);
    }

    @Override
    @Synchronized("serversLock")
    public ServerInfo removeServer(ServerInfo server) {
        return this.servers.remove(server.getName());
    }

    @Override
    @Synchronized("serversLock")
    public boolean removeServersNamed(Collection<String> names) {
        return this.servers.keySet().removeAll(names);
    }

    @Override
    @Synchronized("serversLock")
    public boolean removeServers(Collection<ServerInfo> servers) {
        boolean changed = false;
        for (ServerInfo server : servers) {
            if (null != this.servers.remove(server.getName())) changed = true;
        }
        return changed;
    }
}
