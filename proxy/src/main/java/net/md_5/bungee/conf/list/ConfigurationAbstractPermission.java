package net.md_5.bungee.conf.list;

import net.md_5.bungee.api.config.ConfigurationType;
import net.md_5.bungee.api.config.list.ConfigurationPermission;
import net.md_5.bungee.conf.ConfigurationAbstract;

import java.util.*;

public class ConfigurationAbstractPermission extends ConfigurationAbstract implements ConfigurationPermission {

    public ConfigurationAbstractPermission() {
        super(ConfigurationType.PERMISSION);
    }

    @Override
    public void load() {
        super.load();

        Map<String, Object> permissions = get("permissions", new HashMap<String, Object>());
        if (permissions.isEmpty()) {
            permissions.put("default", Arrays.asList("bungeecord.command.server", "bungeecord.command.list"));
            permissions.put("admin", Arrays.asList("bungeecord.command.alert", "bungeecord.command.end",
                    "bungeecord.command.ip", "bungeecord.command.reload"));
        }
    }

    @Override
    public Collection<String> getPermissions(String group) {
        Collection<String> permissions = get("permissions." + group, null);
        return (permissions == null) ? Collections.EMPTY_SET : permissions;
    }
}
