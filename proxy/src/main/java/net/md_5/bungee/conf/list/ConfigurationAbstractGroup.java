package net.md_5.bungee.conf.list;

import net.md_5.bungee.api.config.ConfigurationType;
import net.md_5.bungee.api.config.list.ConfigurationGroup;
import net.md_5.bungee.conf.ConfigurationAbstract;

import java.util.*;

public class ConfigurationAbstractGroup extends ConfigurationAbstract implements ConfigurationGroup {

    public ConfigurationAbstractGroup() {
        super(ConfigurationType.GROUP);
    }

    @Override
    public void load() {
        super.load();

        Map<String, Object> groups = get("groups", new HashMap<String, Object>());
        if (groups.isEmpty()) {
            groups.put("Fartan", Collections.singletonList("admin"));
        }
    }

    @Override
    public Collection<String> getGroups(String player) {
        Collection<String> groups = get("groups." + player, null);
        Collection<String> ret = (groups == null) ? new HashSet<>() : new HashSet<>(groups);
        ret.add("default");
        return ret;
    }
}
