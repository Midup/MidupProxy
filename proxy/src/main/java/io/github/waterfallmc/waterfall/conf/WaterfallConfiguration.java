package io.github.waterfallmc.waterfall.conf;

import com.google.common.base.Joiner;
import lombok.Getter;
import net.md_5.bungee.conf.ConfigurationAbstract;
import net.md_5.bungee.protocol.ProtocolConstants;

import java.io.File;

public class WaterfallConfiguration extends ConfigurationAbstract {

    private boolean metrics = true;
    private boolean logServerListPing = false;
    private String gameVersion;
    @Getter
    private boolean useNettyDnsResolver = true;
    private int tabThrottle = 1000;

    public WaterfallConfiguration() {
        super(new File("waterfall.yml"));
    }

    @Override
    public void load() {
        super.load();

        metrics = getBoolean("metrics", metrics);
        logServerListPing = getBoolean("log_server_list_ping", logServerListPing);
        // Throttling options
        tabThrottle = getInt("throttling.tab_complete", tabThrottle);
        gameVersion = getString("game_version", "").isEmpty() ? Joiner.on(", ").join(ProtocolConstants.SUPPORTED_VERSIONS) : getString("game_version", "");
        useNettyDnsResolver = getBoolean("use_netty_dns_resolver", useNettyDnsResolver);
    }

    public boolean isMetrics() {
        return metrics;
    }

    public int getTabThrottle() {
        return tabThrottle;
    }

    public boolean isLogServerListPing() {
        return logServerListPing;
    }
}
