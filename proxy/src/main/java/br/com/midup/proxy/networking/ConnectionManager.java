package br.com.midup.proxy.networking;

import br.com.midup.proxy.MidupProxy;
import br.com.midup.proxy.api.data.server.ServerType;
import br.com.midup.proxy.data.server.ServerData;
import br.com.midup.proxy.logging.FormattedLogger;
import br.com.midup.proxy.util.Util;
import lombok.Getter;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.conf.ConfigurationAbstract;
import net.md_5.bungee.config.Configuration;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public final class ConnectionManager {

    public static final String CONFIG_FILE_NAME = "connection.yml";

    private final MidupProxy plugin;

    private final @Getter
    FormattedLogger logger;
    private final Map<ServerData, ServerConnection> serverConnections;
    private final List<ServerConnection> pendingServerConnections;
    private final @Getter
    Map<Integer, ServerData> serverDatas;
    private File configFile;
    private Configuration config;
    private String address;
    private int port;
    private boolean shuttingDown;

    private ServerSocket server;
    private ScheduledTask listenerTask;

    public ConnectionManager(MidupProxy plugin) {
        this.plugin = plugin;
        this.logger = new FormattedLogger(plugin.getParentLogger(), "Net");
        this.serverConnections = new HashMap<>();
        this.pendingServerConnections = new ArrayList<>();
        this.serverDatas = new HashMap<>();
        this.shuttingDown = false;
    }

    public void loadConfiguration() {
        try {
            this.configFile = new File(ConfigurationAbstract.DIRECTORY_CONFIG, CONFIG_FILE_NAME);

            if (!configFile.exists()) {
                plugin.saveResource(CONFIG_FILE_NAME);
            }

            this.config = Util.YAML.load(configFile);

            this.address = config.getString("address");
            if (address == null) {
                this.address = "localhost";
                logger.log(Level.WARNING, "Missing 'address' from %s. Using default %s!", CONFIG_FILE_NAME, address);
            }
            this.port = config.getInt("port");
            if (port == 0) {
                this.port = 6696;
                logger.log(Level.WARNING, "Missing 'port' from %s. Using default %d!", CONFIG_FILE_NAME, port);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e, "An error occurred while loading the server network configuration! Details below:");
        }
    }

    public boolean startListening() {
        if (this.listenerTask != null) {
            return true;
        }

        try {
            InetSocketAddress socketAddress = new InetSocketAddress(address, port);
            logger.log("Binding proxy server listener to %s...", socketAddress);

            this.server = new ServerSocket();
            this.server.bind(socketAddress);

            this.listenerTask = plugin.getProxy().getScheduler().runAsync(plugin, new ConnectionListener());
            logger.log("Proxy server successfully bound. Now accepting new connections...");
            return true;
        } catch (Exception e) {
            logger.log(Level.SEVERE, e, "Failed to bind the server socket to the specific address. Details below:");
            return false;
        }
    }

    public void stopListening() {
        if (this.shuttingDown) {
            return;
        }
        this.shuttingDown = true;

        try {
            if (listenerTask != null) {
                this.listenerTask.cancel();
                this.listenerTask = null;
            }
            synchronized (serverConnections) {
                List<ServerConnection> pendingDisconnects = new ArrayList<>(serverConnections.values());
                for (ServerConnection pendingDisconnect : pendingDisconnects) {
                    pendingDisconnect.disconnect("Proxy closed");
                }
            }
            synchronized (pendingServerConnections) {
                List<ServerConnection> pendingDisconnects = new ArrayList<>(pendingServerConnections);
                for (ServerConnection pendingDisconnect : pendingDisconnects) {
                    pendingDisconnect.disconnect("Proxy closed");
                }
            }
            this.server.close();
        } catch (Exception e) {
            logger.log(Level.SEVERE, e, "Failed to stop server listener. Details below:");
        }
    }

    public void registerPendingServerConnection(ServerConnection serverConnection) {
        synchronized (pendingServerConnections) {
            this.pendingServerConnections.add(serverConnection);
        }
    }

    public ServerConnection getServerConnection(ServerData serverData) {
        synchronized (serverConnections) {
            return serverConnections.get(serverData);
        }
    }

    public void authenticateServerConnection(ServerData serverData, ServerConnection serverConnection) {
        synchronized (pendingServerConnections) {
            serverConnections.remove(serverData);
        }
        synchronized (serverConnections) {
            serverConnections.put(serverData, serverConnection);
        }
    }

    public void unregisterServerConnection(ServerConnection serverConnection) {
        if (serverConnection.getServerData() == null) {
            synchronized (pendingServerConnections) {
                this.pendingServerConnections.remove(serverConnection);
            }
        } else {
            synchronized (serverConnections) {
                this.serverConnections.remove(serverConnection.getServerData());
            }
        }
    }

    public List<ServerData> getServersByType(ServerType serverType) {
        List<ServerData> sDatas = new ArrayList<>();
        for (ServerData data : getServerDatas().values()) {
            if (data.getType() == serverType) {
                sDatas.add(data);
            }
        }
        return sDatas;
    }

    public List<ServerData> getServersByMode(ServerType.ServerMode serverMode) {
        List<ServerData> sDatas = new ArrayList<>();
        for (ServerData data : getServerDatas().values()) {
            if (data.getType().getMode() == serverMode) {
                sDatas.add(data);
            }
        }
        return sDatas;
    }

    public List<ServerData> getServersByTypeAndMode(ServerType serverType, ServerType.ServerMode serverMode) {
        List<ServerData> sDatas = new ArrayList<>();
        for (ServerData data : getServerDatas().values()) {
            if (data.getType() == serverType && data.getType().getMode() == serverMode) {
                sDatas.add(data);
            }
        }
        return sDatas;
    }

    private class ConnectionListener implements Runnable {

        private boolean running;

        private ConnectionListener() {
            this.running = true;
        }

        @Override
        public void run() {
            while (running) {
                try {
                    Socket socket = server.accept();
                    logger.log("Received connection from %s", socket.getInetAddress());
                    ServerConnection serverConnection = new ServerConnection(plugin, ConnectionManager.this, socket,
                            logger);
                    serverConnection.initializeConnection();
                    synchronized (pendingServerConnections) {
                        if (!shuttingDown) {
                            pendingServerConnections.add(serverConnection);
                        }
                    }
                    logger.log("Connection successfully set up! Waiting for handshake...");
                } catch (Exception e) {
                    if (!shuttingDown) {
                        logger.log(Level.SEVERE, e, "Failed to accept new client connection. Details below:");
                    }
                }
            }
        }
    }
}
