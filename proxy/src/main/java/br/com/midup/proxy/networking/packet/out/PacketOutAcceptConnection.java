package br.com.midup.proxy.networking.packet.out;

import br.com.midup.proxy.networking.packet.PacketOut;
import br.com.midup.proxy.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataOutput;

public class PacketOutAcceptConnection extends PacketOut {

    private final String serverName;

    public PacketOutAcceptConnection(String serverName) {
        super(PacketType.Out.ACCEPT_CONNECTION);
        this.serverName = serverName;
    }

    @Override
    public void writePacketData(ByteArrayDataOutput output) {
        output.writeUTF(serverName);
    }
}
