package br.com.midup.proxy.networking.packet;

import br.com.midup.proxy.MidupProxy;
import br.com.midup.proxy.api.data.server.ServerStatus;
import br.com.midup.proxy.data.server.ServerData;
import br.com.midup.proxy.logging.FormattedLogger;
import br.com.midup.proxy.networking.ConnectionManager;
import br.com.midup.proxy.networking.ConnectionState;
import br.com.midup.proxy.networking.ServerConnection;
import br.com.midup.proxy.networking.packet.in.PacketInDisconnect;
import br.com.midup.proxy.networking.packet.in.PacketInHandshake;
import br.com.midup.proxy.networking.packet.out.PacketOutAcceptConnection;

import java.util.logging.Level;

public final class PacketHandler {

    private final MidupProxy plugin;

    private final ConnectionManager connectionManager;
    private final ServerConnection connection;
    private final FormattedLogger logger;

    public PacketHandler(MidupProxy plugin, ServerConnection connection, FormattedLogger logger) {
        this.plugin = plugin;
        this.connectionManager = plugin.getConnectionManager();
        this.connection = connection;
        this.logger = logger;
    }

    public void processHandshake(PacketInHandshake packet) {
        if (packet.getServerType() == null) {
            logger.log(Level.WARNING, "Received an invalid server type!");
            connection.disconnect("Invalid server type!");
            return;
        }

        ServerData serverData = connectionManager.getServerDatas().get(packet.getPort());
        if (serverData == null) {
            logger.log(Level.WARNING, "Missing server type on configuration file!");
            connection.disconnect("Missing server type on proxy side!");
            return;
        }
        logger.setPrefix(serverData.getName());

        if (serverData.getType() != packet.getServerType()) {
            logger.log(Level.WARNING, "Received server type %s, but was expecting %s!", serverData.getType(),
                    packet.getServerType());
            connection.disconnect("Wrong server type!");
            return;
        }

        logger.log("Successfully authenticated as %s!", serverData.getName());
        serverData.setStatus(ServerStatus.CONNECTED);
        this.connection.initializeServerData(serverData);
        this.connection.sendPacket(new PacketOutAcceptConnection(serverData.getName()));
        this.connection.updateConnectionState(ConnectionState.CONNECTED);
        this.connectionManager.authenticateServerConnection(serverData, connection);
    }

    public void handleDisconnect(PacketInDisconnect packet) {
        logger.log("Received disconnect request from the server!");
        connection.updateConnectionState(ConnectionState.DISCONNECTED);
        logger.log("Disconnected! Reason: %s", packet.getReason());
        connection.disconnect(packet.getReason());
    }
}