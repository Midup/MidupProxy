package br.com.midup.proxy.networking.packet;

import com.google.common.io.ByteArrayDataOutput;

public abstract class PacketOut extends Packet {

    public PacketOut(PacketType.PacketTypeOut packetType) {
        super(packetType);
    }

    @Override
    public PacketType.PacketTypeOut getPacketType() {
        return (PacketType.PacketTypeOut) this.packetType;
    }

    public abstract void writePacketData(ByteArrayDataOutput output);
}