package br.com.midup.proxy.networking.packet.out;

import br.com.midup.proxy.networking.packet.PacketOut;
import br.com.midup.proxy.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataOutput;

public final class PacketOutDisconnect extends PacketOut {

    private final String reason;

    public PacketOutDisconnect(String reason) {
        super(PacketType.Out.DISCONNECT);
        this.reason = reason;
    }

    @Override
    public void writePacketData(ByteArrayDataOutput output) {
        output.writeUTF(reason);
    }
}