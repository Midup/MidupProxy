package br.com.midup.proxy.networking;

import br.com.midup.proxy.MidupProxy;
import br.com.midup.proxy.api.data.server.ServerStatus;
import br.com.midup.proxy.data.server.ServerData;
import br.com.midup.proxy.logging.FormattedLogger;
import br.com.midup.proxy.networking.packet.PacketHandler;
import br.com.midup.proxy.networking.packet.PacketIn;
import br.com.midup.proxy.networking.packet.PacketOut;
import br.com.midup.proxy.networking.packet.PacketType;
import br.com.midup.proxy.networking.packet.out.PacketOutDisconnect;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import lombok.Getter;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

public final class ServerConnection {

    private final MidupProxy plugin;

    private final ConnectionManager connectionManager;
    private final FormattedLogger logger;
    private final PacketHandler handler;
    private final ReentrantLock connectionLock;
    private final Socket socket;
    private ConnectionState state;
    private @Getter
    ServerData serverData;
    private ScheduledTask dataReaderTask;
    private DataInputStream input;
    private DataOutputStream output;

    public ServerConnection(MidupProxy plugin, ConnectionManager connectionManager, Socket socket,
                            FormattedLogger parentLogger) {
        this.plugin = plugin;
        this.connectionManager = connectionManager;
        this.logger = new FormattedLogger(parentLogger, "Unknown");

        this.state = ConnectionState.AWAITING_HANDSHAKE;
        this.serverData = null;
        this.handler = new PacketHandler(plugin, this, logger);
        this.connectionLock = new ReentrantLock();

        this.socket = socket;
    }

    public void initializeConnection() throws Exception {
        if (dataReaderTask != null) {
            return;
        }

        this.input = new DataInputStream(socket.getInputStream());
        this.output = new DataOutputStream(socket.getOutputStream());
        this.dataReaderTask = plugin.getProxy().getScheduler().runAsync(plugin, new DataReader());
    }

    public ConnectionState getConnectionState() {
        return state;
    }

    public void initializeServerData(ServerData newData) {
        if (serverData == null) {
            this.serverData = newData;
        }
    }

    public void closeConnection() {
        updateConnectionState(ConnectionState.DISCONNECTED);
        updateServerState(ServerStatus.CLOSED);
        connectionManager.unregisterServerConnection(this);
        connectionLock.lock();
        try {
            this.dataReaderTask.cancel();
            socket.close();
        } catch (Exception e) {
        } finally {
            connectionLock.unlock();
        }
    }

    public void updateServerState(ServerStatus state) {
        if (getServerState() == state) {
            return;
        }
        serverData.setStatus(state);
    }

    public void updateConnectionState(ConnectionState state) {
        if (this.state == state) {
            return;
        }
        this.state = state;
    }

    public void disconnect(String reason) {
        if (state != ConnectionState.DISCONNECTED || serverData.getStatus() != ServerStatus.CLOSED) {
            this.sendPacket(new PacketOutDisconnect(reason));
            logger.log("Disconnected! Reason: %s", reason);
        }
        closeConnection();
    }

    public void sendPacket(PacketOut packet) {
        connectionLock.lock();
        try {
            if (state == ConnectionState.DISCONNECTED) {
                return;
            }

            output.writeByte(packet.getPacketType().getId());
            ByteArrayDataOutput packetBody = ByteStreams.newDataOutput();
            packet.writePacketData(packetBody);
            byte[] packetData = packetBody.toByteArray();
            output.writeShort(packetData.length);
            output.write(packetData);
            output.flush();
        } catch (Exception e) {
            logger.log(Level.SEVERE, e, "Failed to send packet to the midupProxy server! Details below:");
        } finally {
            connectionLock.unlock();
        }
    }

    public String getName() {
        return serverData != null ? serverData.getName() : "Unregistered";
    }

    public int getPort() {
        return serverData != null ? serverData.getPort() : -1;
    }

    public ServerStatus getServerState() {
        return serverData != null ? serverData.getStatus() : ServerStatus.CLOSED;
    }

    private class DataReader extends Thread {

        @Override
        public void run() {
            while (state != ConnectionState.DISCONNECTED) {
                try {
                    byte packetId = input.readByte();
                    PacketType.PacketTypeIn packetType = PacketType.In.getPacketType(packetId);
                    if (packetType == null) {
                        logger.log(Level.WARNING, "Received an invalid packet type from the midupProxy! ID: %d", packetId);
                        disconnect("Invalid packet type");
                        return;
                    }

                    if (state != ConnectionState.CONNECTED) {
                        if (packetType != PacketType.In.HANDSHAKE && packetType != PacketType.In.DISCONNECT) {
                            logger.log(Level.WARNING, "Received a wrong packet while still not authenticated!");
                            disconnect("Illegal packet");
                            return;
                        }
                    } else {
                        if (packetType == PacketType.In.HANDSHAKE) {
                            logger.log(Level.WARNING, "Received authentication packet while already authenticated!");
                            disconnect("Illegal packet");
                            return;
                        }
                    }

                    final PacketIn packet = packetType.createInstance();
                    short packetSize = input.readShort();

                    if (packetSize < 0) {
                        logger.log("Received a negative packet size from the midupProxy! Size: %d", packetSize);
                        disconnect("Invalid packet size");
                        return;
                    }

                    byte[] dataInput = new byte[packetSize];
                    input.readFully(dataInput);
                    packet.readPacketData(ByteStreams.newDataInput(dataInput));
                    packet.processPacketData(handler);
                } catch (Exception e) {
                    if (state != ConnectionState.DISCONNECTED) {
                        if (e instanceof SocketException) {
                            updateConnectionState(ConnectionState.DISCONNECTED);
                        }
                        logger.log(Level.SEVERE, e,
                                "An error occurred while handling incoming data from the midupProxy! Details below:");
                        disconnect("Data handling error");
                    }
                }
            }

        }
    }
}