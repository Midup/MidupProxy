package br.com.midup.proxy.networking.packet;

public abstract class Packet {

    protected final PacketType packetType;

    protected Packet(PacketType packetType) {
        this.packetType = packetType;
    }

    public abstract PacketType getPacketType();
}