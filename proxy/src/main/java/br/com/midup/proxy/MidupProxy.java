package br.com.midup.proxy;

import br.com.midup.proxy.logging.FormattedLogger;
import br.com.midup.proxy.networking.ConnectionManager;
import com.google.common.io.ByteStreams;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.conf.ConfigurationAbstract;

import java.io.*;
import java.util.logging.Level;

public class MidupProxy extends Plugin {

    private static @Getter
    MidupProxy instance;

    private @Getter
    ProxyServer proxy;
    private FormattedLogger logger;
    private @Getter
    ConnectionManager connectionManager;

    public MidupProxy() {
        instance = this;
        this.proxy = ProxyServer.getInstance();
    }

    public void onEnable() {
        this.logger = new FormattedLogger(proxy.getLogger(), null);
        this.connectionManager = new ConnectionManager(this);

        logger.log("Starting networking module...");
        this.connectionManager.loadConfiguration();
        if (!this.connectionManager.startListening()) {
            logger.log("Failed to start network module! Stopping...");
            proxy.stop();
            return;
        }
    }

    public void onDisable() {
        this.connectionManager.stopListening();
    }

    public FormattedLogger getParentLogger() {
        return logger;
    }

    public boolean saveResource(String resource) {
        File targetFile = new File(ConfigurationAbstract.DIRECTORY_CONFIG, resource);
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }
        InputStream in = null;
        OutputStream out = null;
        try {
            in = this.getResourceAsStream(resource);
            out = new FileOutputStream(targetFile);
            ByteStreams.copy(in, out);
            return true;
        } catch (Exception e) {
            logger.log(Level.SEVERE, e, "Failed to save resource file '%s'. Details below:", resource);
            return false;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
    }
}
