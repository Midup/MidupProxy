package br.com.midup.proxy.networking.packet.in;

import br.com.midup.proxy.api.data.server.ServerType;
import br.com.midup.proxy.networking.packet.PacketHandler;
import br.com.midup.proxy.networking.packet.PacketIn;
import br.com.midup.proxy.networking.packet.PacketType;
import com.google.common.io.ByteArrayDataInput;
import lombok.Getter;

public class PacketInHandshake extends PacketIn {

    private @Getter
    ServerType serverType;
    private @Getter
    int port;

    public PacketInHandshake() {
        super(PacketType.In.HANDSHAKE);
    }

    @Override
    public void readPacketData(ByteArrayDataInput input) {
        int serverTypeId = input.readInt();
        this.serverType = ServerType.getServerType(serverTypeId);
        this.port = input.readInt();
    }

    @Override
    public void processPacketData(PacketHandler handler) {
        handler.processHandshake(this);
    }
}