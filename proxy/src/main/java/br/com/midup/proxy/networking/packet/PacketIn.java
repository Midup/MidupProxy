package br.com.midup.proxy.networking.packet;

import com.google.common.io.ByteArrayDataInput;

public abstract class PacketIn extends Packet {

    public PacketIn(PacketType.PacketTypeIn packetType) {
        super(packetType);
    }

    @Override
    public PacketType.PacketTypeIn getPacketType() {
        return (PacketType.PacketTypeIn) this.packetType;
    }

    public abstract void readPacketData(ByteArrayDataInput input);

    public abstract void processPacketData(PacketHandler handler);
}