package br.com.midup.proxy.user;

import br.com.midup.proxy.api.data.server.ServerType;
import br.com.midup.proxy.api.user.Group;
import br.com.midup.proxy.data.user.UserProfile;
import br.com.midup.proxy.data.user.list.UserProfileMain;

import java.util.UUID;

public class User {

    private UUID uniqueId;
    private String name;

    private int cash;
    private Group group;
    private long groupExpire;

    private UserProfile profile;

    public User(UUID uniqueId, String name, ServerType serverType) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.cash = 0;
        this.group = Group.getDefaultGroup();
        this.groupExpire = -1L;
        this.profile = null;
    }

    public UserProfile loadProfile(ServerType serverType) {
        switch (serverType) {
            case MAIN:
                return this.profile = new UserProfileMain(uniqueId, name);
            default:
                throw new IllegalArgumentException("Illegal ServerType");
        }
    }

}
