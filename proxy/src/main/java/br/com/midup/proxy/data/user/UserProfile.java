package br.com.midup.proxy.data.user;

import br.com.midup.proxy.api.data.server.ServerType;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import lombok.Getter;

import java.io.IOException;
import java.util.UUID;

public abstract class UserProfile {

    private UUID uniqueId;
    private String name;

    private @Getter
    ServerType serverType;

    public UserProfile(UUID uniqueId, String name, ServerType serverType) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.serverType = serverType;
    }

    public abstract void writeExtraInfo(ByteArrayDataOutput output) throws IOException;

    public abstract void readExtraInfo(ByteArrayDataInput input) throws IOException;
}
