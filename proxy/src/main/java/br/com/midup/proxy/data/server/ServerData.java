package br.com.midup.proxy.data.server;

import br.com.midup.proxy.api.data.server.ServerStatus;
import br.com.midup.proxy.api.data.server.ServerType;
import br.com.midup.proxy.data.server.list.ServerDataMain;
import br.com.midup.proxy.util.ColorUtil;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.config.ServerInfo;

import java.io.IOException;

public abstract class ServerData {

    private final @Getter
    String name;
    private final @Getter
    int port;
    private final @Getter
    ServerType type;

    private @Getter
    @Setter
    ServerStatus status;
    private @Getter
    @Setter
    ServerInfo info;
    private @Getter
    @Setter
    int players;
    private @Getter
    @Setter
    int maxPlayers;
    private @Getter
    @Setter
    String motd;

    public ServerData(String name, int port, ServerType type) {
        this.name = name;
        this.port = port;
        this.type = type;

        this.status = ServerStatus.CLOSED;
        this.info = null;
        this.players = 0;
        this.maxPlayers = 0;
        this.motd = ColorUtil.GRAY + "Loading server...";
    }

    public static ServerData create(String name, int port, ServerType type) {
        switch (type) {
            case MAIN:
                return new ServerDataMain(name, port);
            default:
                throw new IllegalArgumentException("Illegal ServerType");
        }
    }

    public abstract void writeExtraInfo(ByteArrayDataOutput output) throws IOException;

    public abstract void readExtraInfo(ByteArrayDataInput input) throws IOException;
}
