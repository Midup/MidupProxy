package br.com.midup.proxy.logging;

import java.util.logging.Level;
import java.util.logging.Logger;

public class FormattedLogger {

    private final Logger logger;
    private final FormattedLogger parent;

    private String prefix;
    private String logPrefix;

    public FormattedLogger(Logger logger, String prefix) {
        this.logger = logger;
        this.parent = null;

        this.setPrefix(prefix);
    }

    public FormattedLogger(FormattedLogger parent, String prefix) {
        this.logger = parent.logger;
        this.parent = parent;

        this.setPrefix(prefix);
    }

    public Logger getLogger() {
        return logger;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
        this.logPrefix = this.buildLogPrefix(new StringBuilder()).toString();
        if (logPrefix.length() > 0) {
            this.logPrefix += " ";
        }
    }

    public FormattedLogger getParent() {
        return parent;
    }

    public void log(String message) {
        logger.info(logPrefix + message);
    }

    public void log(String format, Object... args) {
        logger.info(String.format(logPrefix + format, args));
    }

    public void log(Level level, String message) {
        logger.log(level, logPrefix + message);
    }

    public void log(Level level, String message, Throwable ex) {
        logger.log(level, logPrefix + message, ex);
    }

    public void log(Level level, String format, Object... args) {
        logger.log(level, String.format(logPrefix + format, args));
    }

    public void log(Level level, Throwable ex, String format, Object... args) {
        logger.log(level, String.format(format, args), ex);
    }

    private StringBuilder buildLogPrefix(StringBuilder initial) {
        if (prefix != null) {
            initial.insert(0, "[" + prefix + "]");
        }
        return this.parent == null ? initial : parent.buildLogPrefix(initial);
    }
}
