package br.com.midup.proxy.data.user.list;

import br.com.midup.proxy.api.data.server.ServerType;
import br.com.midup.proxy.data.user.UserProfile;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.UUID;

public class UserProfileMain extends UserProfile {

    private @Getter
    @Setter
    int cash;

    public UserProfileMain(UUID uniqueId, String name) {
        super(uniqueId, name, ServerType.MAIN);

        this.cash = 0;
    }

    @Override
    public void writeExtraInfo(ByteArrayDataOutput output) throws IOException {

    }

    @Override
    public void readExtraInfo(ByteArrayDataInput input) throws IOException {

    }
}
