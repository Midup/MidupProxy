package br.com.midup.proxy.data.server.list;

import br.com.midup.proxy.api.data.server.ServerType;
import br.com.midup.proxy.data.server.ServerData;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import java.io.IOException;

public class ServerDataMain extends ServerData {

    public ServerDataMain(String name, int port) {
        super(name, port, ServerType.MAIN);
    }

    @Override
    public void writeExtraInfo(ByteArrayDataOutput output) throws IOException {

    }

    @Override
    public void readExtraInfo(ByteArrayDataInput input) throws IOException {

    }
}
