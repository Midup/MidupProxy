package br.com.midup.proxy.networking.packet;

import br.com.midup.proxy.networking.packet.in.PacketInDisconnect;
import br.com.midup.proxy.networking.packet.in.PacketInHandshake;
import br.com.midup.proxy.networking.packet.out.PacketOutAcceptConnection;
import br.com.midup.proxy.networking.packet.out.PacketOutDisconnect;

import java.util.HashMap;
import java.util.Map;

public interface PacketType {

    public byte getId();

    public Class<? extends Packet> getImplClass();

    public static enum In implements PacketTypeIn {
        HANDSHAKE(0, PacketInHandshake.class),
        DISCONNECT(1, PacketInDisconnect.class);

        private static final Map<Integer, PacketTypeIn> BY_ID = new HashMap<>();

        static {
            for (In type : values()) {
                BY_ID.put((int) type.id, type);
            }
        }

        private final byte id;
        private final Class<? extends PacketIn> implClass;

        private In(int id, Class<? extends PacketIn> implClass) {
            this.id = (byte) id;
            this.implClass = implClass;
        }

        public static PacketTypeIn getPacketType(int id) {
            return BY_ID.get(id);
        }

        @Override
        public byte getId() {
            return id;
        }

        @Override
        public Class<? extends PacketIn> getImplClass() {
            return implClass;
        }

        @Override
        public PacketIn createInstance() throws Exception {
            return implClass.newInstance();
        }
    }

    public static enum Out implements PacketTypeOut {
        ACCEPT_CONNECTION(0, PacketOutAcceptConnection.class),
        DISCONNECT(1, PacketOutDisconnect.class);

        private static final Map<Integer, PacketTypeOut> BY_ID = new HashMap<>();

        static {
            for (Out type : values()) {
                BY_ID.put((int) type.id, type);
            }
        }

        private final byte id;
        private final Class<? extends PacketOut> implClass;

        private Out(int id, Class<? extends PacketOut> implClass) {
            this.id = (byte) id;
            this.implClass = implClass;
        }

        public static PacketTypeOut getPacketType(int id) {
            return BY_ID.get(id);
        }

        @Override
        public byte getId() {
            return id;
        }

        @Override
        public Class<? extends PacketOut> getImplClass() {
            return implClass;
        }
    }

    public static interface PacketTypeIn extends PacketType {

        @Override
        public Class<? extends PacketIn> getImplClass();

        public PacketIn createInstance() throws Exception;
    }

    public static interface PacketTypeOut extends PacketType {

        @Override
        public Class<? extends PacketOut> getImplClass();
    }
}