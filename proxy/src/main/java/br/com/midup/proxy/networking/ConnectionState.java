package br.com.midup.proxy.networking;

public enum ConnectionState {

    DISCONNECTED,
    AWAITING_HANDSHAKE,
    CONNECTED;
}