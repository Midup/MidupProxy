package net.md_5.bungee.api.config;

import lombok.Getter;

public enum ConfigurationType {

    SERVER("server.yml"), LISTENER("listener.yml"), PROXY("proxy.yml"), GROUP("group.yml"), PERMISSION("permission.yml");

    private final @Getter
    String configFileName;

    private ConfigurationType(String configFileName) {
        this.configFileName = configFileName;
    }
}
