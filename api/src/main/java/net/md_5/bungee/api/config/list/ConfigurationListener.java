package net.md_5.bungee.api.config.list;

import net.md_5.bungee.api.config.ListenerInfo;

import java.util.Collection;

public interface ConfigurationListener {

    public Collection<ListenerInfo> getListeners();
}
