package net.md_5.bungee.api.config.list;

import net.md_5.bungee.api.Favicon;

import java.util.Collection;

public interface ConfigurationProxy {

    int getTimeout();

    String getUuid();

    boolean isOnlineMode();

    boolean isLogCommands();

    int getPlayerLimit();

    Collection<String> getDisabledCommands();

    Favicon getFaviconObject();

    //
    // Waterfall Options
    //

    boolean isMetrics();

    boolean isLogServerListPing();

    String getGameVersion();

    boolean isUseNettyDnsResolver();

    int getTabThrottle();
}
