package net.md_5.bungee.api.config.list;

import java.util.Collection;

public interface ConfigurationGroup {

    public Collection<String> getGroups(String player);
}
