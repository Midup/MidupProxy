package net.md_5.bungee.api.config;

import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.net.InetSocketAddress;
import java.util.Collection;

public interface ServerInfo {

    String getName();

    InetSocketAddress getAddress();

    Collection<ProxiedPlayer> getPlayers();

    String getMotd();

    boolean canAccess(CommandSender sender);

    void sendData(String channel, byte[] data);

    boolean sendData(String channel, byte[] data, boolean queue);

    void ping(Callback<ServerPing> callback);
}
