package net.md_5.bungee.api.config.list;

public interface ConfigurationMain {

    public ConfigurationProxy getProxyConfig();

    public ConfigurationServer getServerConfig();

    public ConfigurationListener getListenerConfig();

    public ConfigurationGroup getGroupConfig();

    public ConfigurationPermission getPermissionConfig();
}
