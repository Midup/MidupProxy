package net.md_5.bungee.api.config.list;

import java.util.Collection;

public interface ConfigurationPermission {

    public Collection<String> getPermissions(String group);
}
