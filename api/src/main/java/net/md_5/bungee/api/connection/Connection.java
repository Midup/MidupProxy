package net.md_5.bungee.api.connection;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.protocol.DefinedPacket;

import java.net.InetSocketAddress;

public interface Connection {

    InetSocketAddress getAddress();

    void disconnect(String reason);

    void disconnect(BaseComponent... reason);

    void disconnect(BaseComponent reason);

    boolean isConnected();

    Unsafe unsafe();

    interface Unsafe {

        void sendPacket(DefinedPacket packet);
    }
}
