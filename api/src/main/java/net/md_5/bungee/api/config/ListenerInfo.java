package net.md_5.bungee.api.config;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;

/**
 * Class representing the configuration of a server listener. Used for allowing
 * multiple listeners on different ports.
 */
@Data
@AllArgsConstructor
public class ListenerInfo {

    private final InetSocketAddress host;

    private final String motd;

    private final int maxPlayers;

    private final int tabListSize;

    private final List<String> serverPriority;

    private final boolean forceDefault;

    private final Map<String, String> forcedHosts;

    private final String tabListType;

    private final boolean setLocalAddress;

    private final boolean pingPassthrough;

    private final int queryPort;

    private final boolean queryEnabled;

    private final boolean proxyProtocol;

    @Deprecated
    public ListenerInfo(InetSocketAddress host, String motd, int maxPlayers, int tabListSize, List<String> serverPriority, boolean forceDefault, Map<String, String> forcedHosts, String tabListType, boolean setLocalAddress, boolean pingPassthrough, int queryPort, boolean queryEnabled) {
        this(host, motd, maxPlayers, tabListSize, serverPriority, forceDefault, forcedHosts, tabListType, setLocalAddress, pingPassthrough, queryPort, queryEnabled, false);
    }

    @Deprecated
    public String getDefaultServer() {
        return serverPriority.get(0);
    }

    @Deprecated
    public String getFallbackServer() {
        return (serverPriority.size() > 1) ? serverPriority.get(1) : getDefaultServer();
    }

}
