package net.md_5.bungee.api.connection;

import net.md_5.bungee.api.config.ServerInfo;

public interface Server extends Connection {

    public ServerInfo getInfo();

    public abstract void sendData(String channel, byte[] data);
}
