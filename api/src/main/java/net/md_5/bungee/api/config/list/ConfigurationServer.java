package net.md_5.bungee.api.config.list;

import net.md_5.bungee.api.config.ServerInfo;

import java.util.Collection;
import java.util.Map;

public interface ConfigurationServer {

    public Map<String, ServerInfo> getServers();

    Map<String, ServerInfo> getServersCopy();

    ServerInfo getServerInfo(String name);

    ServerInfo addServer(ServerInfo server);

    boolean addServers(Collection<ServerInfo> servers);

    ServerInfo removeServerNamed(String name);

    ServerInfo removeServer(ServerInfo server);

    boolean removeServersNamed(Collection<String> names);

    boolean removeServers(Collection<ServerInfo> servers);
}
