package net.md_5.bungee.api.config;

import java.util.Collection;

public interface ConfigurationAdapter {

    public void load();

    public int getInt(String path, int def);

    public String getString(String path, String def);

    public boolean getBoolean(String path, boolean def);

    public Collection<?> getList(String path, Collection<?> def);
}
