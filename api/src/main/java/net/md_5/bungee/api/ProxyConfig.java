package net.md_5.bungee.api;

import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;

import java.util.Collection;
import java.util.Map;

@Deprecated
public interface ProxyConfig {

    int getTimeout();

    String getUuid();

    Collection<ListenerInfo> getListeners();

    @Deprecated
    Map<String, ServerInfo> getServers();

    Map<String, ServerInfo> getServersCopy();

    ServerInfo getServerInfo(String name);

    ServerInfo addServer(ServerInfo server);

    boolean addServers(Collection<ServerInfo> servers);

    ServerInfo removeServerNamed(String name);

    ServerInfo removeServer(ServerInfo server);

    boolean removeServersNamed(Collection<String> names);

    boolean removeServers(Collection<ServerInfo> servers);

    boolean isOnlineMode();

    boolean isLogCommands();

    int getPlayerLimit();

    Collection<String> getDisabledCommands();

    @Deprecated
    int getThrottle();

    @Deprecated
    boolean isIpForward();

    @Deprecated
    String getFavicon();

    Favicon getFaviconObject();

    //
    // Waterfall Options
    //

    boolean isMetrics();

    boolean isLogServerListPing();

    String getGameVersion();

    boolean isUseNettyDnsResolver();

    int getTabThrottle();
}
