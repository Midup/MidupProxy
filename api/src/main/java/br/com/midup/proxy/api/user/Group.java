package br.com.midup.proxy.api.user;

import br.com.midup.proxy.util.ColorUtil;
import br.com.midup.proxy.util.ColoredName;

import java.util.*;

public enum Group implements ColoredName {

    DIRECTOR(0, "Diretor", ColorUtil.DARK_RED, true, "dono", "adm", "admin"),
    MANAGER(1, "Gerente", ColorUtil.BLUE, true),
    MODERATOR(2, "Moderador", ColorUtil.DARK_GREEN, true, "mod"),
    BUILDER(3, "Construtor", ColorUtil.DARK_AQUA, true),
    DEVELOPER(4, "Desenvolvedor", ColorUtil.DARK_PURPLE, true, "dev"),
    HELPER(5, "Ajudante", ColorUtil.YELLOW, true),
    YOUTUBER(6, "Youtuber", ColorUtil.RED, false, "yt"),
    PRO(7, "PRO", ColorUtil.GOLD, false),
    MVP(8, "MVP", ColorUtil.AQUA, false),
    VIP(9, "VIP", ColorUtil.GREEN, false),
    USER(10, "Membro", ColorUtil.GRAY, false, "default", "member", "player");

    private static final Map<Integer, Group> BY_ID = new HashMap<>();
    private static final Map<String, Group> BY_ALIAS = new HashMap<>();

    private static final Group DEFAULT_GROUP = Group.USER;

    static {
        for (Group group : values()) {
            BY_ID.put(group.id, group);
            BY_ALIAS.put(group.name.toLowerCase(), group);
            BY_ALIAS.put(group.name().toLowerCase(), group);
            for (String alias : group.aliases) {
                BY_ALIAS.put(alias.toLowerCase(), group);
            }
        }

        DIRECTOR.canSetGroup.addAll(
                Arrays.asList(DIRECTOR, MANAGER, MODERATOR, BUILDER, DEVELOPER, HELPER, YOUTUBER, PRO, MVP, VIP, USER));
        MANAGER.canSetGroup.addAll(Arrays.asList(MODERATOR, BUILDER, DEVELOPER, HELPER, YOUTUBER, PRO, MVP, VIP, USER));
    }

    private final int id;
    private final String name;
    private final String color;
    private final boolean staff;
    private final Set<Group> canSetGroup;
    private final Set<String> aliases;

    private Group(int id, String name, String color, boolean staff, String... aliases) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.staff = staff;
        this.canSetGroup = new HashSet<>();
        this.aliases = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(aliases)));
    }

    public static Group getGroup(int id) {
        return BY_ID.get(id);
    }

    public static Group getGroup(String alias) {
        return BY_ALIAS.get(alias.toLowerCase());
    }

    public static Group getDefaultGroup() {
        return DEFAULT_GROUP;
    }

    public static List<Group> getStaffGroups() {
        List<Group> sGroups = new ArrayList<>();
        for (Group group : Group.values()) {
            if (group.isStaff()) {
                sGroups.add(group);
            }
        }
        return sGroups;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String getColoredName() {
        return color + name;
    }

    public boolean isStaff() {
        return staff;
    }

    public Set<String> getAliases() {
        return aliases;
    }

    public boolean canSetGroup(Group group) {
        return canSetGroup.contains(group);
    }
}