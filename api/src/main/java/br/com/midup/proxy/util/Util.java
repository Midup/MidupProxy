package br.com.midup.proxy.util;

import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class Util {

    public static final YamlConfiguration YAML = (YamlConfiguration) ConfigurationProvider
            .getProvider(YamlConfiguration.class);

    public static int unixTimestamp() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    public static long toJavaTimestamp(int unixTimestamp) {
        return unixTimestamp * 1000;
    }

    public static int toUnixTimestamp(long javaTimestamp) {
        return (int) (javaTimestamp / 1000);
    }

    public static <T> boolean contains(T target, T... elements) {
        for (T element : elements) {
            if (target.equals(element)) {
                return true;
            }
        }
        return false;
    }
}
