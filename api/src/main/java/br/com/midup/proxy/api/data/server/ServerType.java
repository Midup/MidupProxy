package br.com.midup.proxy.api.data.server;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

public enum ServerType {

    MAIN(0, ServerMode.HUB), SKY_WARS(1, ServerMode.MINIGAME), BED_WARS(2, ServerMode.MINIGAME);

    private static final Map<Integer, ServerType> BY_ID = new HashMap<>();
    private static final Map<String, ServerType> BY_ALIAS = new HashMap<>();

    static {
        for (ServerType serverType : values()) {
            BY_ID.put(serverType.id, serverType);
            BY_ALIAS.put(serverType.name().toLowerCase(), serverType);
        }
    }

    private final @Getter
    int id;
    private @Getter
    @Setter
    ServerMode mode;

    private ServerType(int id, ServerMode mode) {
        this.id = id;
        this.mode = mode;
    }

    public static ServerType getServerType(int id) {
        return BY_ID.get(id);
    }

    public static ServerType getServerType(String alias) {
        return BY_ALIAS.get(alias.toLowerCase());
    }

    public enum ServerMode {
        HUB(0), MINIGAME(1);

        private static final Map<Integer, ServerMode> BY_ID = new HashMap<>();
        private static final Map<String, ServerMode> BY_ALIAS = new HashMap<>();

        static {
            for (ServerMode serverMode : values()) {
                BY_ID.put(serverMode.id, serverMode);
                BY_ALIAS.put(serverMode.name().toLowerCase(), serverMode);
            }
        }

        private final @Getter
        int id;

        private ServerMode(int id) {
            this.id = id;
        }

        public static ServerMode getServerMode(int id) {
            return BY_ID.get(id);
        }

        public static ServerMode getServerMode(String alias) {
            return BY_ALIAS.get(alias.toLowerCase());
        }
    }
}
