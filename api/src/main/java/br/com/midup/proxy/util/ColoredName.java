package br.com.midup.proxy.util;

public interface ColoredName {

    public String getColoredName();
}