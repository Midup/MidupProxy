package br.com.midup.proxy.api.data.server;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public enum ServerStatus {

    CLOSED(0), CONNECTED(1), MAINTENANCE(3);

    private static final Map<Byte, ServerStatus> BY_ID = new HashMap<>();

    static {
        for (ServerStatus serverStatus : values()) {
            BY_ID.put(serverStatus.id, serverStatus);
        }
    }

    private @Getter
    byte id;

    private ServerStatus(int id) {
        this.id = (byte) id;
    }

    public static ServerStatus getServerStatus(int id) {
        return BY_ID.get((byte) id);
    }

}