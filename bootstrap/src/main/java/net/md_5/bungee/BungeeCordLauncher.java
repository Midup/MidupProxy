package net.md_5.bungee;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.command.ConsoleCommandSender;

import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class BungeeCordLauncher {

    public static void main(String[] args) throws Exception {
        Security.setProperty("networkaddress.cache.ttl", "30");
        Security.setProperty("networkaddress.cache.negative.ttl", "10");

        OptionParser parser = new OptionParser();
        parser.allowsUnrecognizedOptions();
        parser.acceptsAll(Arrays.asList("v", "version"));
        parser.acceptsAll(Arrays.asList("noconsole"));

        OptionSet options = parser.parse(args);

        if (options.has("version")) {
            System.out.println(Bootstrap.class.getPackage().getImplementationVersion());
            return;
        }

        if (BungeeCord.class.getPackage().getSpecificationVersion() != null && System.getProperty("IReallyKnowWhatIAmDoingISwear") == null) {
            Date buildDate = new SimpleDateFormat("yyyyMMdd").parse(BungeeCord.class.getPackage().getSpecificationVersion());

            Calendar deadline = Calendar.getInstance();
            deadline.add(Calendar.WEEK_OF_YEAR, -4);
            if (buildDate.before(deadline.getTime())) {
                System.err.println("*** Hey! This build is potentially outdated :( ***");
                System.err.println("*** Please check for a new build from https://ci.destroystokyo.com/job/MidupProxy/ ***");
                System.err.println("*** Should this build be outdated, you will get NO support for it. ***");
                System.err.println("*** Server will start in 10 seconds ***");
                Thread.sleep(TimeUnit.SECONDS.toMillis(10));
            }
        }

        BungeeCord bungee = new BungeeCord();
        ProxyServer.setInstance(bungee);
        bungee.getLogger().info("Enabled MidupProxy version " + bungee.getVersion());
        bungee.start();

        if (!options.has("noconsole")) {
            String line;
            while (bungee.isRunning && (line = bungee.getConsoleReader().readLine(">")) != null) {
                if (!bungee.getPluginManager().dispatchCommand(ConsoleCommandSender.getInstance(), line)) {
                    bungee.getConsole().sendMessage(ChatColor.RED + "Command not found");
                }
            }
        }
    }
}
